package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@RequestMapping("/hello")
@CrossOrigin
public class HelloController {

	@GetMapping("/show")
	public ResponseEntity<String> show()
	{
			return new ResponseEntity<String>("Hello", HttpStatus.OK);
	}
}
